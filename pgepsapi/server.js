var express = require('express'),
  app = express(),
  port = process.env.PORT || 3110;

app.all('/', function (req, res, next) {
  console.log('Accessing the secret section ...')
  next() // pass control to the next handler
})
  app.get('/', function(req, res) {
    res.send('Hello world');
  });



  app.listen(port);
  console.log('RESTful API server started on: ' + port);
