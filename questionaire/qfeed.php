<?php
error_reporting(0);
session_start();
ob_start();
if(!$_SESSION['username']){
     header("Location: index.php");
    ob_end_flush();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PGEPS Questionaire</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/questionaire.js"></script>
</head>
<body>
<!--Top Header Begins-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="home.php">Questionaire</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="home.php">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li> <a href="#"><span class="glyphicon glyphicon-user"></span> 
      <?php 
      if($_SESSION['username']){ echo $_SESSION['username']; }
      ?>
      </a></li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
    
  </div>
</nav>
<!--Top Header Ends-->
<div class="container" style="margin-top: 5%;">
  <div class="col-md-4 col-md-offset-4">
    <div class="panel panel-primary">
    <div class="panel-heading">Competitive Exam - Question Creation</div>
    <div class="panel-body">

      <form method="post" action="qform.php" name="qform">

	      <div class="form-group">
  		    <label for="sel1">Select Class:</label>
		      <select class="form-control" id="schoolClass" name="schoolClass">
            <option value="1">I</option>
            <option value="2">II</option>
            <option value="3">III</option>
            <option value="4">IV</option>
            <option value="5">V</option>
            <option value="6">VI</option>
            <option value="7">VII</option>
            <option value="8">VIII</option>
            <option value="9">IX</option>
		      </select>

		      <label for="sel1">Select Subject:</label>
		      <select class="form-control" id="schoolSubject" name="schoolSubject">
			      <option value="English">English</option>
		        <option value="Maths">Maths</option>
		        <option value="Science">Science</option>
		        <option value="Social">Social</option>
		        <option value="Evs">EVS</option>
		      </select>

		      <!--<label for="sel1">No of Questions:</label>
		      <select class="form-control" id="noOfQuestions" name="noOfQuestions">
			      <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option>
		        <option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>
            <option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option>
            <option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option>
            <option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option>
            <option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option>
            <option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option>
            <option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option>
            <option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option>
            <option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option>
		      </select>-->
	        
          
        </div>
        <button type="button" id="classSaveBtn" class="btn btn-primary btn-md">Create</button>
    </form>

			</div>
		</div>
	</div>

</body>
</html>