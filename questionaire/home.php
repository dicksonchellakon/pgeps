<?php
error_reporting(0);
session_start();
ob_start();
if(!$_SESSION['username']){
     header("Location: index.php");
    ob_end_flush();
}
include_once 'common.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PGEPS Questionaire</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/questionaire.js"></script>
</head>
<body>
<!--Top Header Begins-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Questionaire</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="home.php">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li> <a href="#"><span class="glyphicon glyphicon-user"></span> 
      <?php 
      if($_SESSION['username']){ echo $_SESSION['username']; }
      ?>
      </a></li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
    
  </div>
</nav>
<!--Top Header Ends-->
<div class="container" style="margin-top: 3%;">

    
  <div class="col-md-6 ">
    <div class="panel panel-primary">
      <div class="panel-heading">Home</div>
      <div class="panel-body">

        <ul class="list-group">
          <li class="list-group-item"><a href="qfeed.php" class="btn btn-info" role="button">Competitive Exam - Question Creation
</a></li>
        </ul>
        
      </div>
    </div>
  </div>
<?php

?>
  <div class="col-md-6 ">
    <div class="panel panel-primary">
      <div class="panel-heading">List</div>
      <div class="panel-body">

        
        <?php
        $classSubList = getClassSubscriptionList();
         if(count($classSubList)) {
             echo '<div class="list-group">';
             foreach ($classSubList as $key => $value){             
                echo '<a href="questions.php?qid='.$value['SubscriptionCode'].'" class="list-group-item">'.$value['SubscriptionCode'].' <span class="badge">'.$value['TotalQuestions'].'</span></a>';
             }
             echo '</div>';
         }
         else {
             echo '<ul class="list-group"><li class="list-group-item">No Content Available</li></ul>';
         }
        ?>
        
      </div>
    </div>
  </div>
  
</div>
</body>
</html>