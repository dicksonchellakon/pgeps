$(document).ready(function() {


$( "#classSaveBtn" ).on( "click", function( event ) {
  $.ajax({
    url: 'saveClass.php',
    data: {
        schoolClass: $('#schoolClass :selected').val(),
        schoolSubject: $('#schoolSubject :selected').val()
    },
    success: function( result ) {
        if(result == 'exist') {
          alert('Already exist');
          return;
        } 

        if(result != 'false') {
          window.location.href = 'questions.php?qid='+ result;
        }
    }
  }); 
});

$('#questionSubmit').on('click', function(event){
  event.preventDefault();

  $.ajax({
    url: 'saveQuestion.php',
    data: {
        question: encodeURIComponent($('#txtbxquestion').val()),
        option1: encodeURIComponent($('#txtbxoption1').val()),
        option2: encodeURIComponent($('#txtbxoption2').val()),
        option3: encodeURIComponent($('#txtbxoption3').val()),
        option4: encodeURIComponent($('#txtbxoption4').val()),
        answer: $('#drpdwnanswer :selected').val(),
        subscriptionCode: $('#SubscriptionCode').val()
    },
    success: function( result ) {
        if(result == 'exist') {
          alert('Already exist');
          return;
        } 

        if(result != 'false') {
          //window.location.href = 'questions.php?qid='+ result;
        }
    }
  }); 

});

$("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#questionslist li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });



});