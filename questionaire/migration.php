<?php
include_once 'config/db.php';
$database = new Database();
$dbconn = $database->getConnection();
error_reporting(0);
date_default_timezone_set('UTC');
include_once 'common.php';
$subject = trim($_REQUEST['subject']);
$class = trim($_REQUEST['class']);
$action = trim(strtolower($_REQUEST['action']));

if($subject && $class) {
    $getListOfOldQuestions = getListOfOldQuestions($subject, $class);
    
    print '<pre>';
    print_r($getListOfOldQuestions);
    print '</pre>';

    if(count($getListOfOldQuestions)) {
      
      $subscriptionCode = date('Y') .'-'. $class .'-'. $subject;
      $classExists = isClassSubsriptionExist($subscriptionCode);
      if(count($classExists)) {
        
        if($action == 'replace') {
          $query = "DELETE FROM questions WHERE SubscriptionCode=:subscriptionCode";
          $stmt = $dbconn->prepare($query);
          $stmt->bindParam(":subscriptionCode", $subscriptionCode);
          if($stmt->execute()) {
            echo '<br/>Deleted the questions belong to - '.$subscriptionCode;
          }
        }

        $subCodeSplit = explode("-", $subscriptionCode);
        $query = "INSERT INTO
                    questions
                SET
                    SubscriptionCode=:subscriptionCode, QuestionNo=:questno, Questions=:questions, 
                    Options1=:option1, Options2=:option2, Options3=:option3, Options4=:option4,
                    Answer=:answer, Created=:created";

        // prepare query
        $stmt = $dbconn->prepare($query);
        // bind values
        
        $stmt->bindParam(":subscriptionCode", $subscriptionCode);
        $time = time();
        $questInc = $classExists['TotalQuestions'];
        if($action == 'replace') {
          $questInc = 0;
        }
        
        foreach($getListOfOldQuestions as $key => $value) {
          $questionNo = $subCodeSplit[2].'-'.(string)(++$questInc);
          $stmt->bindParam(":questno", $questionNo);
          $stmt->bindParam(":questions", $value['questions']);
          $stmt->bindParam(":option1", $value['option1']);
          $stmt->bindParam(":option2", $value['option2']);
          $stmt->bindParam(":option3", $value['option3']);
          $stmt->bindParam(":option4", $value['option4']);
          $stmt->bindParam(":answer", $value['answer']);
          $stmt->bindParam(":created", $time);
          if($stmt->execute()) {
            echo '<br/>inserted the question-'.(string)($questInc);
          }
        }


                     // update the total questions
              $query = 'UPDATE ClassSubscription
                      SET
                          TotalQuestions = :totalquestions
                      WHERE
                          SubscriptionCode = :SubscriptionCode';
          
              // prepare query statement
              $stmt = $dbconn->prepare($query);
              $stmt->bindParam(':totalquestions', $questInc);
              $stmt->bindParam(':SubscriptionCode', $subscriptionCode);

              if($stmt->execute()) {
                echo 'update the totalquestion-'.(string)($questInc);
              }

      }
      else {
        // INSERT the Class Subscription
        $query = "INSERT INTO
                    ClassSubscription
                SET
                    SubscriptionCode=:subscriptionCode, Standard=:standard, Subject=:subject, Created=:created";

        // prepare query
        $stmt = $dbconn->prepare($query);
        // bind values
          
        $stmt->bindParam(":subscriptionCode", $subscriptionCode);
        $stmt->bindParam(":standard", $class);
        $stmt->bindParam(":subject", $subject);
        $stmt->bindParam(":created", time());

        if($stmt->execute()) {
          echo 'New Class Subscription code:= '.$subscriptionCode;
        }

        // INSERT the Questions
        $subCodeSplit = explode("-", $subscriptionCode);
        //$questionNo = $subCodeSplit[2].'-'.(string)($classExists['TotalQuestions'] + 1);
            $query = "INSERT INTO
                        questions
                    SET
                        SubscriptionCode=:subscriptionCode, QuestionNo=:questno, Questions=:questions, 
                        Options1=:option1, Options2=:option2, Options3=:option3, Options4=:option4,
                        Answer=:answer, Created=:created";

            // prepare query
            $stmt = $dbconn->prepare($query);
            // bind values
            
            $stmt->bindParam(":subscriptionCode", $subscriptionCode);
            $time = time();$questInc = 0;
            
            foreach($getListOfOldQuestions as $key => $value) {
              $questionNo = $subCodeSplit[2].'-'.(string)(++$questInc);
              $stmt->bindParam(":questno", $questionNo);
              $stmt->bindParam(":questions", $value['questions']);
              $stmt->bindParam(":option1", $value['option1']);
              $stmt->bindParam(":option2", $value['option2']);
              $stmt->bindParam(":option3", $value['option3']);
              $stmt->bindParam(":option4", $value['option4']);
              $stmt->bindParam(":answer", $value['answer']);
              $stmt->bindParam(":created", $time);
              if($stmt->execute()) {
                echo '<br/>inserted the question-'.(string)($questInc);
              }
            }
            
              // update the total questions
              $query = 'UPDATE ClassSubscription
                      SET
                          TotalQuestions = :totalquestions
                      WHERE
                          SubscriptionCode = :SubscriptionCode';
          
              // prepare query statement
              $stmt = $dbconn->prepare($query);
              $stmt->bindParam(':totalquestions', $questInc);
              $stmt->bindParam(':SubscriptionCode', $subscriptionCode);

              if($stmt->execute()) {
                echo 'update the totalquestion-'.(string)($questInc);
              }
      }
    }
    else {
      echo 'No questions in the old question bank.';
    }
}
else {
    echo 'Enter the Subject and Class';
}
exit;
?>