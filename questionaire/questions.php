<?php
error_reporting(0);
session_start();
ob_start();
if(!$_SESSION['username']){
     header("Location: index.php");
    ob_end_flush();
}
include_once 'common.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PGEPS Questionaire</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/questionaire.js?v=3"></script>
</head>
<body>
<!--Top Header Begins-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Questionaire</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="home.php">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li> <a href="#"><span class="glyphicon glyphicon-user"></span> 
      <?php 
      if($_SESSION['username']){ echo $_SESSION['username']; }
      ?>
      </a></li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
    
  </div>
</nav>
<!--Top Header Ends-->

  <div class="container" style="margin-top: 2%;">
<?php
if(isset($_REQUEST['qid']) && trim($_REQUEST['qid'])  ) {
  $classSubOne = isClassSubsriptionExist(trim($_REQUEST['qid']));
  if(count($classSubOne)) {
    $listOfQuestions = getAllQuestions(trim($_REQUEST['qid']));
 ?>

 <div class="row">
    <div class="col-sm-3"><b>Class:</b> <?php echo $classSubOne['Standard'];?></div>
    <div class="col-sm-3"><b>Subject:</b> <?php echo $classSubOne['Subject'];?></div>
    <div class="col-sm-3"><b>No.of Questions:</b> <?php echo $classSubOne['TotalQuestions'];?></div>
 
    <div class="col-sm-3"><a href="question.php?operation=add&qid=<?php echo $classSubOne['SubscriptionCode'];?>" >Add Question</a></div>
  </div>
  <div class="col-md-12">
    <div class="panel panel-primary"> 
    <div class="panel-body">
      <?php if(count($listOfQuestions)) { ?>
      <input class="form-control" id="myInput" type="text" placeholder="Search questions...">
      <ul class="list-group" id="questionslist" style="margin:10px 0 0 0;">
        <?php
          foreach($listOfQuestions as $question) {
            $splitStr = explode('-', $question['QuestionNo']);
            echo '<li class="list-group-item">'.$splitStr[1].'. '.$question['Questions'];
            echo '<br/>';
            echo '<b>A.</b> '.$question['Options1'];
            echo ' <b>B.</b> '.$question['Options2'];
            echo ' <b>C.</b> '.$question['Options3'];
            echo ' <b>D.</b> '.$question['Options4'];
            echo '<span class="badge">Delete</span>';
            echo '<span class="badge"><a href="question.php?operation=edit&qid='. $classSubOne['SubscriptionCode'].'&q='.$splitStr[1].'">Edit</a></span>';
            echo '<span class="badge">Answer: '.returnAnswer($question['Answer']).'</span>';
            
            echo '</li>';
          }
        ?>
      </ul>
      <?php } ?>
    </div>
	</div>

 <?php 
  }
  else {
    echo '<div class="alert alert-danger">
  <strong>Not Found!</strong> Indicates the question id not available.
</div>';
  }
}
else {
 echo '<div class="alert alert-danger">
  <strong>Invalid!</strong> Indicates the invalid question id provided.
</div>';
}
?>

  
	</div>
</body>
</html>