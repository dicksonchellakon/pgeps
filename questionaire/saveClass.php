<?php
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
 
include_once 'config/db.php';
$database = new Database();
$dbconn = $database->getConnection();
error_reporting(0);
date_default_timezone_set('UTC');


$schoolClass = trim($_REQUEST['schoolClass']);
$schoolSubject = trim($_REQUEST['schoolSubject']);
$subscriptionCode = date('Y') .'-'. $schoolClass .'-'. $schoolSubject;


// select all query
$query = 'SELECT SubscriptionCode from ClassSubscription where SubscriptionCode="'. $subscriptionCode.'" LIMIT 1';
 
// prepare query statement
$stmt = $dbconn->prepare($query);
 
// execute query
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if(trim($row['SubscriptionCode'])) {
  echo 'exist';
  exit;
}
else {

  // query to insert record
  $query = "INSERT INTO
              ClassSubscription
          SET
              SubscriptionCode=:subscriptionCode, Standard=:standard, Subject=:subject, Created=:created";

  // prepare query
  $stmt = $dbconn->prepare($query);
  // bind values
    
  $stmt->bindParam(":subscriptionCode", $subscriptionCode);
  $stmt->bindParam(":standard", $schoolClass);
  $stmt->bindParam(":subject", $schoolSubject);
  $stmt->bindParam(":created", time());

  if($stmt->execute()){
    echo $subscriptionCode;
    exit;
  } 
}

echo 'false';
exit;
?>