<?php
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
 
include_once 'config/db.php';
$database = new Database();
$dbconn = $database->getConnection();
error_reporting(0);
date_default_timezone_set('UTC');
include_once 'common.php';

$question = urldecode(trim($_REQUEST['question']));
$option1 = urldecode(trim($_REQUEST['option1']));
$option2 = urldecode(trim($_REQUEST['option2']));
$option3 = urldecode(trim($_REQUEST['option3']));
$option4 = urldecode(trim($_REQUEST['option4']));
$answer = trim($_REQUEST['answer']);
$subscriptionCode = trim($_REQUEST['subscriptionCode']);
$subCodeSplit = explode("-", $subscriptionCode);

$classSubOne = isClassSubsriptionExist($subscriptionCode);

$questionNo = $subCodeSplit[2].'-'.(string)($classSubOne['TotalQuestions'] + 1); 


  $query = "INSERT INTO
              questions
          SET
              SubscriptionCode=:subscriptionCode, QuestionNo=:questno, Questions=:questions, 
              Options1=:option1, Options2=:option2, Options3=:option3, Options4=:option4,
              Answer=:answer, Created=:created";

  // prepare query
  $stmt = $dbconn->prepare($query);
  // bind values
  
  $stmt->bindParam(":subscriptionCode", $subscriptionCode);
  $stmt->bindParam(":questno", $questionNo);
  $stmt->bindParam(":questions", $question);
  $stmt->bindParam(":option1", $option1);
  $stmt->bindParam(":option2", $option2);
  $stmt->bindParam(":option3", $option3);
  $stmt->bindParam(":option4", $option4);
  $stmt->bindParam(":answer", $answer);
  $stmt->bindParam(":created", time());

  if($stmt->execute()) {

    // update query
    $query = 'UPDATE ClassSubscription
            SET
                TotalQuestions = TotalQuestions + 1
            WHERE
                SubscriptionCode = :SubscriptionCode';
 
    // prepare query statement
    $stmt = $dbconn->prepare($query);
    $stmt->bindParam(':SubscriptionCode', $subscriptionCode);

    if($stmt->execute()) {
      echo 'true';
      exit;
    }
    echo 'false';
    exit;
  }
echo 'false';
exit;
?>