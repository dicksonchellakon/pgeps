<?php
error_reporting(0);
session_start();
ob_start();
if(!$_SESSION['username']){
     header("Location: index.php");
    ob_end_flush();
}
include_once 'common.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PGEPS Questionaire</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/questionaire.js?v=3"></script>
</head>
<body>
<!--Top Header Begins-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Questionaire</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="home.php">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li> <a href="#"><span class="glyphicon glyphicon-user"></span> 
      <?php 
      if($_SESSION['username']){ echo $_SESSION['username']; }
      ?>
      </a></li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
    
  </div>
</nav>
<!--Top Header Ends-->

  <div class="container" style="margin-top: 2%;">
<?php
if(isset($_REQUEST['qid']) && trim($_REQUEST['qid'])  ) {
  $classSubOne = isClassSubsriptionExist(trim($_REQUEST['qid']));
  if(count($classSubOne)) {
    $editQuestion = $editoption1 = $editoption2 = $editoption3 = $editoption4 = '';
    if($_REQUEST['q'] && $_REQUEST['operation'] == 'edit') {
        $splitStr = explode('-', $classSubOne['SubscriptionCode']);
        $aQuestion = getAQuestion(trim($_REQUEST['qid']), $splitStr[2].'-'.trim($_REQUEST['q']));
        $editQuestion = $aQuestion['Questions'];
        $editoption1 = $aQuestion['Options1'];
        $editoption2 = $aQuestion['Options2'];
        $editoption3 = $aQuestion['Options3'];
        $editoption4 = $aQuestion['Options4'];
        $editAnswer = $aQuestion['Answer'];
    }
 ?>

 <div class="row">
    <div class="col-sm-3"><b>Class Subscription:</b>
     <a href="questions.php?qid=<?php echo $classSubOne['SubscriptionCode'];?>"><?php echo $classSubOne['SubscriptionCode'];?></a></div>
    <div class="col-sm-3"><b>Class:</b> <?php echo $classSubOne['Standard'];?></div>
    <div class="col-sm-3"><b>Subject:</b> <?php echo $classSubOne['Subject'];?></div>
    <?php if($_REQUEST['operation'] == 'add') { ?>
    <div class="col-sm-3"><b>No.of Questions:</b> <?php echo $classSubOne['TotalQuestions'];?></div>
    <?php } ?>
    <?php if($_REQUEST['q'] && $_REQUEST['operation'] == 'edit') { ?>
    <div class="col-sm-3"><b>Question No:</b> <?php echo $_REQUEST['q'];?></div>
    <?php } ?>
  </div>
  <div class="col-md-12">
    <div class="panel panel-primary"> 
    <div class="panel-body">
      


    <form method="post" action="" name="qform">
      <div class="form-group">
        <label for="pwd">Question:</label>
        <textarea class="form-control" rows="2" id="txtbxquestion" name="question" required><?php echo trim($editQuestion); ?></textarea>
      </div>

      <div class="form-group">
        <label for="pwd">Option A:</label>
        <input type="text" name="option1" size="100" id="txtbxoption1" class="form-control input-sm" value="<?php echo trim($editoption1); ?>" required>
      </div>

      <div class="form-group">
        <label for="pwd">Option B:</label>
        <input type="text" name="option1" size="100" id="txtbxoption2" class="form-control input-sm" value="<?php echo trim($editoption2); ?>" required>
      </div>

      <div class="form-group">
        <label for="pwd">Option C:</label>
        <input type="text" name="option1" size="100" id="txtbxoption3" class="form-control input-sm" value="<?php echo trim($editoption3); ?>" required>
      </div>

      <div class="form-group">
        <label for="pwd">Option D:</label>
        <input type="text" name="option1" size="100" class="form-control input-sm" id="txtbxoption4" value="<?php echo trim($editoption4); ?>" required>
      </div>

      <div class="form-group">
        <label for="pwd">Answer:</label>
        <select class="form-control" id="drpdwnanswer" name="answer" required> 
			    <option value="1" <?php echo $editAnswer == 1 ? ' selected="selected"' : '';?> >Option A</option>
		        <option value="2" <?php echo $editAnswer == 2 ? ' selected="selected"' : '';?>>Option B</option>
		        <option value="3" <?php echo $editAnswer == 3 ? ' selected="selected"' : '';?>>Option C</option>
		        <option value="4" <?php echo $editAnswer == 4 ? ' selected="selected"' : '';?>>Option D</option>
		    </select>
      </div>
      <button type="submit" class="btn btn-primary btn-md" id="questionSubmit">Save</button>
      <input type="hidden" id="SubscriptionCode" value="<?php echo trim($_REQUEST['qid']);  ?>" />
    </form>


</div>
		</div>

 <?php 
  }
  else {
    echo '<div class="alert alert-danger">
  <strong>Not Found!</strong> Indicates the question id not available.
</div>';
  }
}
else {
 echo '<div class="alert alert-danger">
  <strong>Invalid!</strong> Indicates the invalid question id provided.
</div>';
}
?>

  
	</div>
</body>
</html>