<?php
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
 
include_once 'config/db.php';
$database = new Database();
$dbconn = $database->getConnection();
error_reporting(0);
date_default_timezone_set('UTC');


function getClassSubscriptionList() {
  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, TotalQuestions from ClassSubscription';
  
  // prepare query statement
  $stmt = $dbconn->prepare($query);
  
  // execute query
  $stmt->execute();
  
  $classList = $classRow = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        //extract($row);
        $classRow =array(
          "SubscriptionCode" => $row['SubscriptionCode'],
            "TotalQuestions" => $row['TotalQuestions']
        );
        array_push($classList, $classRow);
    }
    return $classList;
}

function isClassSubsriptionExist($qid) {
  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, Standard, Subject, TotalQuestions from ClassSubscription WHERE SubscriptionCode = ? LIMIT 1';
  
  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $stmt->bindParam(1, trim($qid));
  
  // execute query
  $stmt->execute();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  $classRow = array();
  if($row) {
    $classRow = array(
      "SubscriptionCode" => $row['SubscriptionCode'],
      "Standard" => $row['Standard'],
      "Subject" => $row['Subject'],
      "TotalQuestions" => $row['TotalQuestions']
    );
  }
  return $classRow;
}

function getAllQuestions($qid) {

  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, QuestionNo, Questions, Options1, Options2, Options3, Options4, Answer, Created from questions WHERE SubscriptionCode = ? ';
  
  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $stmt->bindParam(1, trim($qid));
  
  // execute query
  $stmt->execute();
  
  $questionsList = $questionRow = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        //extract($row);
        $questionRow =array(
          "SubscriptionCode" => $row['SubscriptionCode'],
          "QuestionNo" => $row['QuestionNo'],
          "Questions" => $row['Questions'],
          "Options1" => $row['Options1'],
          "Options2" => $row['Options2'],
          "Options3" => $row['Options3'],
          "Options4" => $row['Options4'],
          "Answer" => $row['Answer'],
          "Created" => $row['Created']
        );
        array_push($questionsList, $questionRow);
    }
  return $questionsList;
}


function returnAnswer($ans) {
  switch(trim($ans)){
    case 1:
      return 'A';
    case 2:
      return 'B';
    case 3:
      return 'C';
    case 4:
      return 'D';
    default:
      return 'NA';
  }
}

function getAQuestion($qid, $q) {
  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, QuestionNo, Questions, Options1, Options2, Options3, Options4, Answer, Created from questions WHERE SubscriptionCode = ? AND QuestionNo = ? LIMIT 1';
  
  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $stmt->bindParam(1, trim($qid));
  $stmt->bindParam(2, trim($q));
  
  // execute query
  $stmt->execute();
  
  $questionRow = array();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  if($row) {
     $questionRow =array(
          "SubscriptionCode" => $row['SubscriptionCode'],
          "QuestionNo" => $row['QuestionNo'],
          "Questions" => $row['Questions'],
          "Options1" => $row['Options1'],
          "Options2" => $row['Options2'],
          "Options3" => $row['Options3'],
          "Options4" => $row['Options4'],
          "Answer" => $row['Answer'],
          "Created" => $row['Created']
        );
  }
 
  return $questionRow;
}


function getListOfOldQuestions($subject, $class) {
    global $dbconn;

  // select all query
  $query = 'SELECT class, subject, questions, option1, option2, option3, option4, answer from insertquestion WHERE subject = ? AND class = ?';
  
  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $stmt->bindParam(1, trim($subject));
  $stmt->bindParam(2, trim($class));
  
  // execute query
  $stmt->execute();

  $questionsList = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($questionsList, array(
          "class" => $row['class'],
          "subject" => $row['subject'],
          "questions" => $row['questions'],
          "option1" => $row['option1'],
          "option2" => $row['option2'],
          "option3" => $row['option3'],
          "option4" => $row['option4'],
          "answer" => $row['answer']
        ));
    }
  return $questionsList;
}
?>