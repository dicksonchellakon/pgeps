<!DOCTYPE html>
<html lang="en">
<head>
  <title>PGEPS Questionaire</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<?php
error_reporting(0);
session_start();
if($_SESSION['username']) {
    unset($_SESSION['username']);
    session_destroy();
}
?>
<div class="container" style="margin-top: 5%;">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-primary">
            <div class="panel-body">
            
            <div class="alert alert-danger">
  <strong>Logout!</strong> Please <a href="/questionaire">login</a> again.
</div>
            
        </div>
    </div>
</div>

</body>
</html>

