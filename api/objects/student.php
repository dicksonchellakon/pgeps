<?php
class Student{
 
    // database connection and table name
    private $conn;
    private $table_name = "Student";
 
    // object properties
    public $RegistrationNo;
    public $FirstName;
    public $LastName;
    public $Gender;
    public $DateOfBirth;
    public $Class;
    public $Religion;
    public $Community;
    public $Address;
    public $City;
    public $Pincode;
    public $NeedTransport;
    public $DistanceFromSchool;

 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    // read products
  function read() {
 
    // select all query
    $query = "SELECT
                 s.RegistrationNo, s.FirstName, s.LastName, s.Gender, s.DateOfBirth, s.Class, s.Religion, s.Community,
                 s.Address, s.City, s.Pincode, s.NeedTransport, s.DistanceFromSchool, s.Created
            FROM
                " . $this->table_name . " s
                
            ORDER BY
                s.Created DESC";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
  }
  function readLatestId() {
 
    // select all query
    $query = "SELECT
                 s.RegistrationId
            FROM
                " . $this->table_name . " s
                
            ORDER BY
                s.RegistrationId DESC LIMIT 1";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt->fetchColumn();
  }

  // create product
  function create() {
 
    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                RegistrationNo=:RegistrationNo, FirstName=:FirstName, LastName=:LastName, Gender=:Gender, DateOfBirth=:DateOfBirth, Class=:Class, 
                Religion=:Religion, Community=:Community, Address=:Address, City=:City, Pincode=:Pincode,
                NeedTransport=:NeedTransport, DistanceFromSchool=:DistanceFromSchool, LastModified=:LastModified,
                Created=:Created";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    /*$this->FirstName = htmlspecialchars(strip_tags($this->FirstName));
    $this->LastName = htmlspecialchars(strip_tags($this->LastName));
    $this->Gender=htmlspecialchars(strip_tags($this->Gender));
    $this->DateOfBirth=htmlspecialchars(strip_tags($this->DateOfBirth));
    $this->Class=htmlspecialchars(strip_tags($this->Class));
    $this->created=htmlspecialchars(strip_tags($this->created));
 */
  date_default_timezone_set("Asia/Kolkata"); 
    // bind values
    $this->RegistrationNo = date('Y') . date('m') . $this->readLatestId() + 1;
    $stmt->bindParam(":RegistrationNo",  $this->RegistrationNo);
    $stmt->bindParam(":FirstName", htmlspecialchars(strip_tags($this->FirstName)));
    $stmt->bindParam(":LastName", htmlspecialchars(strip_tags($this->LastName)));
    $stmt->bindParam(":Gender", htmlspecialchars(strip_tags($this->Gender)));
    $stmt->bindParam(":DateOfBirth", htmlspecialchars(strip_tags($this->DateOfBirth)));
    $stmt->bindParam(":Class", $this->Class);
    $stmt->bindParam(":Religion", $this->Religion);
    $stmt->bindParam(":Community", $this->Community);
    $stmt->bindParam(":Address", $this->Address);
    $stmt->bindParam(":City", $this->City);
    $stmt->bindParam(":Pincode", $this->Pincode);
    $stmt->bindParam(":NeedTransport", $this->NeedTransport);
    $stmt->bindParam(":DistanceFromSchool", $this->DistanceFromSchool);
    $stmt->bindParam(":LastModified", time());
    $stmt->bindParam(":Created", time());
    
  //$this->conn->beginTransaction(); 
  $result = $stmt->execute();
   
    //    $this->conn->commit(); 
         
    // execute query
    if($result){
        return $this->conn->lastInsertId();
    }
    else{
        return false;
    }
}


}