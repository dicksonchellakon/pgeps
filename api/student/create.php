<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/student.php';
 
$database = new Database();
$db = $database->getConnection();
 
$student = new Student($db);
 
// get posted data
//var_dump($_GET);
//$data = json_decode(file_get_contents("php://input"));
 
// set product property values
$student->FirstName = $_GET['FirstName'];//$data->FirstName;
$student->LastName = $_GET['LastName'];//$data->LastName;
$student->Gender = $_GET['Gender'];//$data->Gender;
$student->DateOfBirth = $_GET['DateOfBirth'];//$data->DateOfBirth;
$student->Class = $_GET['Class'];//$data->Class;
$student->Religion = $_GET['Religion'];//$data->Religion;
$student->Community = $_GET['Community'];//$data->Community;
$student->Address = $_GET['Address'];//$data->Address;
$student->City = $_GET['City'];//$data->City;
$student->Pincode = $_GET['Pincode'];//$data->Pincode;
$student->NeedTransport = $_GET['NeedTransport'];//$data->NeedTransport;
$student->DistanceFromSchool = $_GET['DistanceFromSchool'];//$data->DistanceFromSchool;

 
// create the product
if($student->create()) {
    $student_arr['message'] = 'Student was created.';
}
 
// if unable to create the product, tell the user
else {
    $student_arr['message'] = 'Unable to create student';
}
echo json_encode($student_arr);
?>