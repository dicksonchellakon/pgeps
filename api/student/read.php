<?php
// show error reporting
ini_set('display_errors', 1);
error_reporting(E_ALL);
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/student.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

 
// initialize object
$student = new Student($db);
 
// query products
$stmt = $student->read();
$num = $stmt->rowCount();
 
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $products_arr=array();
    $products_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $product_item=array(
            "registrationno" => $RegistrationNo,
            "name" => $FirstName .' '. $LastName,
            "gender" => $Gender,
            "dateofbirth" => $DateOfBirth,
            "class" => $Class,
            "religion" => $Religion,
            "community" => $Community,
            "address" => $Address,
            "city" => $City,
            "pincode" => $Pincode,
            "needtransport" => $NeedTransport,
            "schooldistance" => $DistanceFromSchool,
            "reated" => $Created
        );
 
        array_push($products_arr["records"], $product_item);
    }
 
    echo json_encode($products_arr);
}
 
else {
    echo json_encode(
        array("message" => "No students found.")
    );
}
?>