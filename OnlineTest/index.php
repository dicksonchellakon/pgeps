<?php
session_start();
ob_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Velankanni Group of Schools - Competitive Exam</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/onlinetest.js?v=2"></script>
</head>
<body>

<div class="container" style="margin-top: 5%;">
  <div class="col-md-4 col-md-offset-4">
    <div class="panel panel-primary">
      <div class="panel-heading">Student Details</div>

      <div class="panel-body">
        <form role="form" action="enroll.php" method="post">

          <div class="row">
            <div class="form-group col-xs-12">
              <label for="rollno"><span class="text-danger" style="margin-right:5px;">*</span>Admission No:</label>
              <div class="input-group">
                <input class="form-control" id="txtAdmissionNo" type="text" name="admissionno" placeholder="Admission No" required/>
                <!--<span class="input-group-btn">
                  <label class="btn btn-primary"><span class="glyphicon glyphicon-user" aria-hidden="true"></label>
                </span>
                </span>-->
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-xs-12">
              <label for="rollno"><span class="text-danger" style="margin-right:5px;">*</span>Roll No:</label>
              <div class="input-group">
                <input class="form-control" id="txtRollNo" type="text" name="rollno" placeholder="Roll No" required/>
                <!--<span class="input-group-btn">
                  <label class="btn btn-primary"><span class="glyphicon glyphicon-user" aria-hidden="true"></label>
                </span>
                </span>-->
              </div>
            </div>
          </div>


<!-- Student Name Field -->
          <div class="row">
            <div class="form-group col-xs-12">
            <label for="student name"><span class="text-danger" style="margin-right:5px;">*</span>Student Name</label>
            <div class="input-group">
              <input class="form-control" id="txtStudentName" type="text" name="studentname" placeholder="Student Name" required/>
              <!--<span class="input-group-btn">
              <label class="btn btn-primary"><span class="glyphicon glyphicon-lock" aria-hidden="true"></label></span>
              </span>-->
            </div>
            </div>
          </div>

<!-- Class Field -->
        <div class="row">
          <div class="form-group col-xs-12">
            <label for="class"><span class="text-danger" style="margin-right:5px;">*</span>Class:</label>
            <div class="input-group">
            <select class="form-control" id="drpdwnStandard" name="standard" required>
                <option value="1">I</option>
                <option value="2">II</option>
                <option value="3">III</option>
                <option value="4">IV</option>
                <option value="5">V</option>
                <option value="6">VI</option>
                <option value="7">VII</option>
                <option value="8">VIII</option>
                <option value="9">IX</option>
		        </select>
<!--<span class="input-group-btn">
<label class="btn btn-primary"><span class="glyphicon glyphicon-user" aria-hidden="true"></label></span></span>-->
	         </div>
          </div>
      </div>


          <!-- Section Field -->
          <div class="row">
            <div class="form-group col-xs-12">
            <label for="section"><span class="text-danger" style="margin-right:5px;">*</span>Section</label>
            <div class="input-group">
            <!--<input class="form-control" id="section" type="text" name="section" placeholder="Section" required/>
             -->
            <select class="form-control" id="drpdwnSection" name="section" required>
              <option value="A">A</option>
              <option value="B">B</option>
              <option value="C">C</option>
              <option value="D">D</option>
              <option value="E">E</option>
              <option value="F">F</option>
              <option value="G">G</option>
              <option value="H">H</option>
            </select>
            <!--<span class="input-group-btn">
          <label class="btn btn-primary"><span class="glyphicon glyphicon-lock" aria-hidden="true"></label></span>
          </span>-->
                </div>
              </div>
            </div>

         <!-- Login Button -->

      <div class="row">
        <div class="form-group col-xs-4">
          <button class="btn btn-primary" type="submit" name="submit" id="enrollSubmit">Submit</button>
        </div>
      </div>

    </form>

  <!-- End of Login Form -->


        </div>

 </div>
</div>
</div>

</body>

</html>
