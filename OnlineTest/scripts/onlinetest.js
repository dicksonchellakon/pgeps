$(document).ready(function() {


$('#enrollSubmit').on('click', function(event) {
  event.preventDefault();

  $.ajax({
    url: 'enroll.php',
    data: {
        StudentName: encodeURIComponent($('#txtStudentName').val()),
        AdmissionNo: $('#txtAdmissionNo').val(),
        RollNo: $('#txtRollNo').val(),
        Standard: $('#drpdwnStandard').val(),
        Section: $('#drpdwnSection').val(),
    },
    success: function( result ) {
      if(result == 'true'){
        window.location.href = 'exam.php?admissionno='+ $('#txtAdmissionNo').val();
      }
    }
  });
});

$('.tab_english, .tab_maths, .tab_science, .tab_social, .tab_evs').on('click', function(event) {
  $('#examCompleteionAlert').addClass('hide');
});

$('#saveEnglishAnswers').on('click', function(event) {
  var fruits = new Array();
  var checkSucc = true;
  $( "li.questions-english" ).each(function() {
      var SplitStr = $(this).attr('id');
      var splitNo = SplitStr.split('_');
      var radVal = $(this).find( "input[type=radio][name=optradio_"+splitNo[2]+"]:checked" ).val();
      if(radVal === undefined && checkSucc === true){
        alert('Please answer question: '+splitNo[2]);
        checkSucc = false;
      }
      fruits.push(splitNo[2]+'-'+radVal);
  });
  if(checkSucc) {
    $.ajax({
      url: 'saveanswers.php',
      method: 'POST',
      data: {
          Answer: JSON.stringify(fruits),
          AdmissionNo: $('#hdnAdmissionNo').val(),
          RollNo: $('#hdnRollNo').val(),
          SubscriptionCode: $('#tab_english').find('#hdnEnglishSubscriptionCode').val(),
      },
      success: function( result ) {
        if(result == 'true') {
          $('#examCompleteionAlert').removeClass('hide').find('#subjectlabel').html('English');
        }
      }
    });
  }
});

$('#saveMathsAnswers').on('click', function(event) {
  var mathsAns = new Array();
  var checkSucc = true;
  $( "li.questions-maths" ).each(function() {
      var SplitStr = $(this).attr('id');
      var splitNo = SplitStr.split('_');
      var radVal = $(this).find( "input[type=radio][name=optradio_"+splitNo[2]+"]:checked" ).val();
      if(radVal === undefined && checkSucc === true){
        alert('Please answer question: '+splitNo[2]);
        checkSucc = false;
      }
      mathsAns.push(splitNo[2]+'-'+radVal);
  });
  if(checkSucc){
    $.ajax({
      url: 'saveanswers.php',
      method: 'POST',
      data: {
          Answer: JSON.stringify(mathsAns),
          AdmissionNo: $('#hdnAdmissionNo').val(),
          RollNo: $('#hdnRollNo').val(),
          SubscriptionCode: $('#tab_maths').find('#hdnMathsSubscriptionCode').val(),
      },
      success: function( result ) {alert(result);
        if(result == 'true'){
          //window.location.href = 'exam.php?admissionno='+ $('#txtAdmissionNo').val();
        }
      }
    });
  }

});


$('#saveScienceAnswers').on('click', function(event) {
  var scienceAns = new Array();
  var checkSucc = true;
  $( "li.questions-science" ).each(function() {
      var SplitStr = $(this).attr('id');
      var splitNo = SplitStr.split('_');
      var radVal = $(this).find( "input[type=radio][name=optradio_"+splitNo[2]+"]:checked" ).val();
      if(radVal === undefined && checkSucc === true){
        alert('Please answer question: '+splitNo[2]);
        checkSucc = false;
      }
      scienceAns.push(splitNo[2]+'-'+radVal);
  });
  if(checkSucc){
    $.ajax({
      url: 'saveanswers.php',
      method: 'POST',
      data: {
          Answer: JSON.stringify(scienceAns),
          AdmissionNo: $('#hdnAdmissionNo').val(),
          RollNo: $('#hdnRollNo').val(),
          SubscriptionCode: $('#tab_science').find('#hdnScienceSubscriptionCode').val(),
      },
      success: function( result ) {alert(result);
        if(result == 'true'){
          //window.location.href = 'exam.php?admissionno='+ $('#txtAdmissionNo').val();
        }
      }
    });
  }

});


$('#saveSocialAnswers').on('click', function(event) {
  var socialAns = new Array();
  var checkSucc = true;
  $( "li.questions-social" ).each(function() {
      var SplitStr = $(this).attr('id');
      var splitNo = SplitStr.split('_');
      var radVal = $(this).find( "input[type=radio][name=optradio_"+splitNo[2]+"]:checked" ).val();
      if(radVal === undefined && checkSucc === true){
        alert('Please answer question: '+splitNo[2]);
        checkSucc = false;
      }
      socialAns.push(splitNo[2]+'-'+radVal);
  });
  if(checkSucc){
    $.ajax({
      url: 'saveanswers.php',
      method: 'POST',
      data: {
          Answer: JSON.stringify(socialAns),
          AdmissionNo: $('#hdnAdmissionNo').val(),
          RollNo: $('#hdnRollNo').val(),
          SubscriptionCode: $('#tab_social').find('#hdnSocialSubscriptionCode').val(),
      },
      success: function( result ) {alert(result);
        if(result == 'true'){
          //window.location.href = 'exam.php?admissionno='+ $('#txtAdmissionNo').val();
        }
      }
    });
  }

});


$('#saveEvsAnswers').on('click', function(event) {
  var evsAns = new Array();
  var checkSucc = true;
  $( "li.questions-evs" ).each(function() {
      var SplitStr = $(this).attr('id');
      var splitNo = SplitStr.split('_');
      var radVal = $(this).find( "input[type=radio][name=optradio_"+splitNo[2]+"]:checked" ).val();
      if(radVal === undefined && checkSucc === true){
        alert('Please answer question: '+splitNo[2]);
        checkSucc = false;
      }
      evsAns.push(splitNo[2]+'-'+radVal);
  });
  if(checkSucc){
    $.ajax({
      url: 'saveanswers.php',
      method: 'POST',
      data: {
          Answer: JSON.stringify(evsAns),
          AdmissionNo: $('#hdnAdmissionNo').val(),
          RollNo: $('#hdnRollNo').val(),
          SubscriptionCode: $('#tab_evs').find('#hdnEvsSubscriptionCode').val(),
      },
      success: function( result ) {alert(result);
        if(result == 'true'){
          //window.location.href = 'exam.php?admissionno='+ $('#txtAdmissionNo').val();
        }
      }
    });
  }

});



});
