<?php include_once 'common.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Velankanni Group of Schools - Competitive Exam</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/onlinetest.js?v=5"></script>
</head>
<body >
<?php $AdmissionNo = $_REQUEST['admissionno']; ?>
	<!--Top Header Begins-->
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php">Online Test</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="exam.php?admissionno=<?php echo $AdmissionNo; ?>">Exam</a></li>
				<li class="active"><a href="result.php?admissionno=<?php echo $AdmissionNo; ?>">Results</a></li>
			</ul>
		</div>
	</nav>
	<!--Top Header Ends-->


<div class="container" >


<h1 style="text-align:center;""font-size:300%;"><font color="blue">VELANKANNI GROUP OF SCHOOLS</font></h1>

<h5 style ="text-align:center;"><font color="blue">Ashok Nagar &#9733;K.K Nagar &#9733;  Kodungaiyur&#9733; Kundrathur </font> </h5>
<h4 class="text-center">Cummulative Test Result</h4>
<?php
	$admissionno = trim($_REQUEST['admissionno']);
	if($admissionno) {
		$studentDetails = getStudentDetailsByAdmissionNo($admissionno);
?>
<div class="row">
    <div class="col-sm-3"><b>Admission No:</b> <?php echo $studentDetails['AdmissionNo'];?></div>
		<div class="col-sm-3"><b>Roll No:</b> <?php echo $studentDetails['RollNo'];?></div>
    <div class="col-sm-3"><b>Student Name:</b> <?php echo $studentDetails['StudentName'];?></div>
    <div class="col-sm-3"><b>Class and Section :</b> <?php echo $studentDetails['Standard'].' - '.$studentDetails['Section'];?></div>
</div>
<?php }

	$studentAllAnswers = getStudentAllAnswers($admissionno);//print_r($studentAllAnswers);
	$arrayBySubject = array();
	$Marks = array();
	foreach ($studentAllAnswers as $key1 => $value1) {

		$questionsWithCorrectAns = getQuestionsCorrectAnswers($value1['SubscriptionCode']);
		$answerInArray = unserialize($value1['Answer']);
		if(count($questionsWithCorrectAns)) {
			$SplitSub = explode('-', $value1['SubscriptionCode']);
			array_push($arrayBySubject,  $questionsWithCorrectAns);
			$score = $quest = 0;
			foreach ($questionsWithCorrectAns as $key2 => $value2) { //Loop the question with correct answers
				foreach ($value2 as $key3 => $value3) {
					$SplitSub1 = explode('-', $value3['QuestionNo']);
					$quest++;
					foreach ($answerInArray as $key4 => $value4) { //Loop the Student answer
						$SplitAns = explode('-', $value4);
						if($SplitSub1[1] == $SplitAns[0]) {
							if($value3['Answer'] == $SplitAns[1]) {
								$score++;
								break;
							}
						}
					}
				}

			}
			$Marks[$SplitSub[2]] = array($quest, $score);

		}
	}
?>

<table class="table table-bordered">
    <thead>
      <tr>
        <th>Subject</th>
				<th>Questions</th>
        <th>Marks</th>
      </tr>
    </thead>
    <tbody>
		<?php
			$totalScore = 0;
		foreach ($Marks as $key5 => $value5) {
			echo '<tr><td>'.$key5.'</td><td>'.$value5[0].'</td><td>'.$value5[1].'</td></tr>';
			$totalScore += $value5[1];
		}
		echo '<tr><td><b>Total</b></td><td>&nbsp;</td><td><b>'.$totalScore.'</b></td></tr>';
		?>
    </tbody>
  </table>

</div>


</body>
</html>
