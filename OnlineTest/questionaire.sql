-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2018 at 08:10 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `questionaire`
--

-- --------------------------------------------------------

--
-- Table structure for table `feedquestion`
--

CREATE TABLE IF NOT EXISTS `feedquestion` (
  `class` varchar(10) NOT NULL,
  `subject` varchar(20) NOT NULL,
  `noques` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedquestion`
--

INSERT INTO `feedquestion` (`class`, `subject`, `noques`) VALUES
('2', 'English', 2);

-- --------------------------------------------------------

--
-- Table structure for table `insertquestion`
--

CREATE TABLE IF NOT EXISTS `insertquestion` (
`sno` int(5) NOT NULL,
  `class` varchar(10) NOT NULL,
  `subject` varchar(20) NOT NULL,
  `noq` int(5) NOT NULL,
  `questions` varchar(250) NOT NULL,
  `option1` varchar(100) NOT NULL,
  `option2` varchar(100) NOT NULL,
  `option3` varchar(100) NOT NULL,
  `option4` varchar(100) NOT NULL,
  `answer` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insertquestion`
--

INSERT INTO `insertquestion` (`sno`, `class`, `subject`, `noq`, `questions`, `option1`, `option2`, `option3`, `option4`, `answer`) VALUES
(1, '', '', 0, 'Malaria caused by', 'mosquito', 'insect', 'ant', 'spider', 'a'),
(2, '', '', 0, 'what', 'aaaaa', 'bbbb', 'cccccccccc', 'dddddddddd', 'a'),
(3, '1', 'English', 5, 'what ', 'aaa', 'bbb', 'ccc', 'dddd', '1'),
(4, '1', 'English', 5, 'How', 'aaa', 'bbbb', 'cccc', 'dddd', '2');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`LoginId` int(11) NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 NOT NULL,
  `active` tinyint(4) NOT NULL,
  `role` tinytext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`LoginId`, `username`, `password`, `active`, `role`) VALUES
(1, 'admin', 'admin', 1, 'admin'),
(2, 'jeba', 'jeba123', 1, 'teacher');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `Qid` int(11) NOT NULL,
  `QuestionNo` varchar(20) NOT NULL,
  `Questions` varchar(255) NOT NULL,
  `Options1` varchar(50) NOT NULL,
  `Options2` varchar(50) NOT NULL,
  `Options3` varchar(50) NOT NULL,
  `Options4` varchar(50) NOT NULL,
  `Answer` tinyint(4) NOT NULL,
  `Created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `studentanswer`
--

CREATE TABLE IF NOT EXISTS `studentanswer` (
`qid` int(10) NOT NULL,
  `optionsid` int(5) NOT NULL,
  `rollno` int(10) NOT NULL,
  `class` varchar(10) NOT NULL,
  `section` varchar(5) NOT NULL,
  `studentname` varchar(100) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `answers` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studentanswer`
--

INSERT INTO `studentanswer` (`qid`, `optionsid`, `rollno`, `class`, `section`, `studentname`, `subject`, `answers`) VALUES
(1, 0, 0, '', '', '', '', ''),
(2, 0, 2105, '1 ', 'A ', 'arul ', 'English ', '<br />\r\n<b>Notice</b>:  Undefined index: options i'),
(3, 0, 2105, '1 ', 'A ', 'arul ', 'English ', '<br />\r\n<b>Notice</b>:  Undefined index: options i'),
(4, 0, 2105, '1 ', 'A ', 'arul ', 'English ', 'dddd'),
(5, 1, 2105, '1 ', 'A ', 'arul ', 'English ', 'dddd'),
(6, 1, 2105, '1 ', 'A ', 'arul ', 'English ', 'dddd'),
(7, 1, 2105, '1 ', 'A ', 'arul ', 'English ', 'dddd'),
(8, 1, 2105, '1 ', 'A ', 'arul ', 'English ', 'dddd'),
(9, 0, 2105, '1 ', 'A ', 'arul ', ' ', 'cccc');

-- --------------------------------------------------------

--
-- Table structure for table `studentlist`
--

CREATE TABLE IF NOT EXISTS `studentlist` (
`sid` int(11) NOT NULL,
  `rollno` int(6) NOT NULL,
  `studentname` varchar(100) NOT NULL,
  `class` varchar(10) NOT NULL,
  `section` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studentlist`
--

INSERT INTO `studentlist` (`sid`, `rollno`, `studentname`, `class`, `section`) VALUES
(1, 2105, 'arul', '1', 'A'),
(2, 2105, 'arul', '1', 'A'),
(3, 2105, 'arul', '1', 'A'),
(4, 2105, 'arul', '1', 'A'),
(5, 2105, 'arul', '1', 'A'),
(6, 2105, 'arul', '1', 'A'),
(7, 2105, 'arul', '1', 'A'),
(8, 2105, 'arul', '1', 'A'),
(9, 2105, 'arul', '1', 'A'),
(10, 2105, 'arul', '1', 'A'),
(11, 2105, 'arul', '1', 'A'),
(12, 2105, 'arul', '1', 'A'),
(13, 2105, 'arul', '1', 'A'),
(14, 2105, 'arul', '1', 'A'),
(15, 2105, 'arul', '1', 'A'),
(16, 2105, 'arul', '1', 'A'),
(17, 2105, 'arul', '1', 'A'),
(18, 2105, 'arul', '1', 'A'),
(19, 2105, 'arul', '1', 'A'),
(20, 2105, 'arul', '1', 'A'),
(21, 2105, 'arul', '1', 'A'),
(22, 2105, 'arul', '1', 'A'),
(23, 2105, 'arul', '1', 'A'),
(24, 2105, 'arul', '1', 'A'),
(25, 2105, 'arul', '1', 'A'),
(26, 2105, 'arul', '1', 'A'),
(27, 2105, 'arul', '1', 'A'),
(28, 2105, 'arul', '1', 'A'),
(29, 2105, 'arul', '1', 'A'),
(30, 2105, 'arul', '1', 'A'),
(31, 2105, 'arul', '1', 'A'),
(32, 2105, 'arul', '1', 'A'),
(33, 2105, 'arul', '1', 'A'),
(34, 2105, 'arul', '1', 'A'),
(35, 2105, 'arul', '1', 'A'),
(36, 2105, 'arul', '1', 'A'),
(37, 2105, 'arul', '1', 'A'),
(38, 2105, 'arul', '1', 'A'),
(39, 2105, 'arul', '1', 'A'),
(40, 2105, 'arul', '1', 'A'),
(41, 2105, 'arul', '1', 'A'),
(42, 2105, 'arul', '1', 'A'),
(43, 2105, 'arul', '1', 'A'),
(44, 2105, 'arul', '1', 'A'),
(45, 2105, 'arul', '1', 'A'),
(46, 2105, 'arul', '1', 'A'),
(47, 2105, 'arul', '1', 'A'),
(48, 2105, 'arul', '1', 'A'),
(49, 2105, 'arul', '1', 'A'),
(50, 2105, 'arul', '1', 'A'),
(51, 2105, 'arul', '1', 'A'),
(52, 2105, 'arul', '1', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `insertquestion`
--
ALTER TABLE `insertquestion`
 ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`LoginId`);

--
-- Indexes for table `studentanswer`
--
ALTER TABLE `studentanswer`
 ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `studentlist`
--
ALTER TABLE `studentlist`
 ADD PRIMARY KEY (`sid`,`rollno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `insertquestion`
--
ALTER TABLE `insertquestion`
MODIFY `sno` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `LoginId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `studentanswer`
--
ALTER TABLE `studentanswer`
MODIFY `qid` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `studentlist`
--
ALTER TABLE `studentlist`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
