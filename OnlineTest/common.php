<?php
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");

include_once 'config/db.php';
$database = new Database();
$dbconn = $database->getConnection();
//error_reporting(0);
date_default_timezone_set('UTC');


function getClassSubscriptionList() {
  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, TotalQuestions from ClassSubscription';

  // prepare query statement
  $stmt = $dbconn->prepare($query);

  // execute query
  $stmt->execute();

  $classList = $classRow = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        //extract($row);
        $classRow =array(
          "SubscriptionCode" => $row['SubscriptionCode'],
            "TotalQuestions" => $row['TotalQuestions']
        );
        array_push($classList, $classRow);
    }
    return $classList;
}

function getStudentDetailsByAdmissionNo($admissionno) {
  global $dbconn;

  // select all query
  $query = 'SELECT AdmissionNo, RollNo, StudentName, Standard, Section from StudentsEnrolled WHERE AdmissionNo = ? LIMIT 1';

  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $stmt->bindParam(1, trim($admissionno));

  // execute query
  $stmt->execute();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  $studentRow = array();
  if($row) {
    $studentRow = array(
      "AdmissionNo" => $row['AdmissionNo'],
      "RollNo" => $row['RollNo'],
      "StudentName" => $row['StudentName'],
      "Standard" => $row['Standard'],
      "Section" => $row['Section']
    );
  }
  return $studentRow;
}

function getAllQuestions($Standard) {

  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, QuestionNo, Questions, Options1, Options2, Options3, Options4, Answer, Created from questions WHERE SubscriptionCode LIKE :Standard ';

  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $Standard = "%-$Standard-%";
  $stmt->bindParam(':Standard', $Standard);

  // execute query
  $stmt->execute();

  $questionsList = $questionRow = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $questionRow =array(
      "SubscriptionCode" => $row['SubscriptionCode'],
      "QuestionNo" => $row['QuestionNo'],
      "Questions" => $row['Questions'],
      "Options1" => $row['Options1'],
      "Options2" => $row['Options2'],
      "Options3" => $row['Options3'],
      "Options4" => $row['Options4'],
      "Answer" => $row['Answer'],
      "Created" => $row['Created']
    );
    array_push($questionsList, $questionRow);
  }
  return $questionsList;
}


function returnAnswer($ans) {
  switch(trim($ans)){
    case 1:
      return 'A';
    case 2:
      return 'B';
    case 3:
      return 'C';
    case 4:
      return 'D';
    default:
      return 'NA';
  }
}

function getAQuestion($qid, $q) {
  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, QuestionNo, Questions, Options1, Options2, Options3, Options4, Answer, Created from questions WHERE SubscriptionCode = ? AND QuestionNo = ? LIMIT 1';

  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $stmt->bindParam(1, trim($qid));
  $stmt->bindParam(2, trim($q));

  // execute query
  $stmt->execute();

  $questionRow = array();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  if($row) {
     $questionRow =array(
          "SubscriptionCode" => $row['SubscriptionCode'],
          "QuestionNo" => $row['QuestionNo'],
          "Questions" => $row['Questions'],
          "Options1" => $row['Options1'],
          "Options2" => $row['Options2'],
          "Options3" => $row['Options3'],
          "Options4" => $row['Options4'],
          "Answer" => $row['Answer'],
          "Created" => $row['Created']
        );
  }

  return $questionRow;
}

function getStudentAllAnswers($admissionno) {
  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, RollNo, Answer, Created from StudentsAnswers WHERE AdmissionNo = ?';

  // prepare query statement
  $stmt = $dbconn->prepare($query);
  $stmt->bindParam(1, trim($admissionno));

  // execute query
  $stmt->execute();

  $answersList = $answersRow = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

     $answersRow =array(
          "SubscriptionCode" => $row['SubscriptionCode'],
          "RollNo" => $row['RollNo'],
          "Answer" => $row['Answer'],
          "Created" => $row['Created']
        );
    array_push($answersList, $answersRow);
  }

  return $answersList;
}

function getQuestionsCorrectAnswers($SubscriptionCode) {

  global $dbconn;

  // select all query
  $query = 'SELECT SubscriptionCode, QuestionNo, Answer, Created from questions WHERE SubscriptionCode = ? ';

  // prepare query statement
  $stmt = $dbconn->prepare($query);

  $stmt->bindParam(1, $SubscriptionCode);

  // execute query
  $stmt->execute();

  $answersList = $answersRow = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $SubscriptionCode = $row['SubscriptionCode'];
    array_push($answersRow, array(
      "QuestionNo" => $row['QuestionNo'],
      "Answer" => $row['Answer'],
      "Created" => $row['Created']
    ));
  }

  $answersList[$SubscriptionCode] = $answersRow;
  return $answersList;
}

?>
