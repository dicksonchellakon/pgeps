<?php
ob_start();
if(!trim($_REQUEST['admissionno'])){
     header("Location: index.php");
    ob_end_flush();
}
include_once 'common.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Velankanni Group of Schools - Competitive Exam</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/onlinetest.js?v=7"></script>
</head>
<body>
<?php $AdmissionNo = $_REQUEST['admissionno']; ?>
  <!--Top Header Begins-->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Online Test</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="exam.php?admissionno=<?php echo $AdmissionNo; ?>">Exam</a></li>
        <li><a href="result.php?admissionno=<?php echo $AdmissionNo; ?>">Results</a></li>
      </ul>
    </div>
  </nav>
  <!--Top Header Ends-->
<div class="container" style="margin-top: 5%;">
  <?php
    $studentDetails = getStudentDetailsByAdmissionNo($AdmissionNo);
    if(count($studentDetails)) {
?>
<div class="row">
    <div class="col-sm-3"><b>Admission No:</b> <?php echo $studentDetails['AdmissionNo'];?></div>
		<div class="col-sm-3"><b>Roll No:</b> <?php echo $studentDetails['RollNo'];?></div>
    <div class="col-sm-3"><b>Student Name:</b> <?php echo $studentDetails['StudentName'];?></div>
    <div class="col-sm-3"><b>Class and Section :</b> <?php echo $studentDetails['Standard'].' - '.$studentDetails['Section'];?></div>
</div>
<?php
      $allQuestions = getAllQuestions(trim($studentDetails['Standard']));
      $arrEnglishQuestions = $arrMathsQuestions = $arrScienceQuestions = $arrSocialQuestions = $arrEvsQuestions  = array();
      foreach ($allQuestions as $key => $value) {
        $SplitStr = explode('-',$value['SubscriptionCode']);
        switch (strtolower(trim($SplitStr[2]))) {
          case 'english':
            array_push($arrEnglishQuestions,$value);
            break;
          case 'maths':
              array_push($arrMathsQuestions,$value);
            break;
          case 'science':
            array_push($arrScienceQuestions,$value);
            break;
          case 'social':
            array_push($arrSocialQuestions,$value);
            break;
          case 'evs':
            array_push($arrEvsQuestions,$value);
            break;
          default:
            # code...
            break;
        }
        # code...
      }
    }


  ?>
  <input type="hidden" id="hdnAdmissionNo" value="<?php echo $studentDetails['AdmissionNo']; ?>"/>
  <input type="hidden" id="hdnRollNo" value="<?php echo $studentDetails['RollNo']; ?>"/>
  <ul class="nav nav-tabs">
    <?php if(count($arrEnglishQuestions)) { ?>
    <li><a data-toggle="tab" class="tab_english" href="#tab_english">English</a></li>
    <?php
    }
    if(count($arrMathsQuestions)) {
    ?>
    <li ><a data-toggle="tab" class="tab_maths" href="#tab_maths">Maths</a></li>
    <?php
    }
    if(count($arrScienceQuestions)) {
    ?>
    <li ><a data-toggle="tab" class="tab_science" href="#tab_science">Science</a></li>
    <?php
    }
    if(count($arrSocialQuestions)) {
    ?>
    <li ><a data-toggle="tab" class="tab_social" href="#tab_social">Social</a></li>
    <?php
    }
    if(count($arrEvsQuestions)) {
    ?>
    <li><a data-toggle="tab" class="tab_evs" href="#tab_evs">EVS</a></li>
    <?php
    } ?>
  </ul>
<?php

if(!count($allQuestions)) {
  echo '<div class="alert alert-info"><strong>Info!</strong> No Exam available now.</div>';
}
?>
<div class="alert alert-success hide" id="examCompleteionAlert">
  <strong>Congratulation!</strong> You have completed <span id="subjectlabel">English</span> exam.
</div>
  <div class="tab-content">
    <div id="tab_english" class="tab-pane fade in active">
      <?php if(count($arrEnglishQuestions)) { ?>
      <ul class="list-group">

        <?php foreach ($arrEnglishQuestions as $key => $value) {
              $englishSubscriptionCode = $value['SubscriptionCode'];
              $qno = explode('-', $value['QuestionNo']);
          echo '<li class="list-group-item questions-english" id="questions_'.$qno[0].'_'.$qno[1].'"><b>'.$qno[1].'. '.$value['Questions'].'</b>';
          echo '<br><br>';
          echo '<label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="1">'.$value['Options1'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="2">'.$value['Options2'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="3">'.$value['Options3'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="4">'.$value['Options4'].'</label>';
          echo '</li>';
        } ?>
        <li class="list-group-item">
          <input type="hidden" id="hdnEnglishSubscriptionCode" value="<?php echo $englishSubscriptionCode; ?>"/>
          <button type="button" id="saveEnglishAnswers" class="btn btn-primary">Save Answers</button></li>
      </ul>
      <?php }else{ ?>
        <div class="alert alert-info">
          <strong>Info!</strong> No exam available in English.
        </div>
      <?php } ?>
    </div>


    <div id="tab_maths" class="tab-pane fade ">
      <?php if(count($arrMathsQuestions)) { ?>
      <ul class="list-group">

        <?php foreach ($arrMathsQuestions as $key => $value) {
              $mathsSubscriptionCode = $value['SubscriptionCode'];
              $qno = explode('-', $value['QuestionNo']);
          echo '<li class="list-group-item questions-maths" id="questions_'.$qno[0].'_'.$qno[1].'"><b>'.$qno[1].'. '.$value['Questions'].'</b>';
          echo '<br><br>';
          echo '<label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="1">'.$value['Options1'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="2">'.$value['Options2'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="3">'.$value['Options3'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="4">'.$value['Options4'].'</label>';
          echo '</li>';
        } ?>
        <li class="list-group-item">
          <input type="hidden" id="hdnMathsSubscriptionCode" value="<?php echo $mathsSubscriptionCode; ?>"/>
          <button type="button" id="saveMathsAnswers" class="btn btn-primary">Save Answers</button></li>
      </ul>
      <?php }else{ ?>
        <div class="alert alert-info">
          <strong>Info!</strong> No exam available in Maths.
        </div>
      <?php } ?>
    </div>


    <div id="tab_science" class="tab-pane fade ">
      <?php if(count($arrScienceQuestions)) { ?>
      <ul class="list-group">

        <?php foreach ($arrScienceQuestions as $key => $value) {
              $scienceSubscriptionCode = $value['SubscriptionCode'];
              $qno = explode('-', $value['QuestionNo']);
          echo '<li class="list-group-item questions-science" id="questions_'.$qno[0].'_'.$qno[1].'"><b>'.$qno[1].'. '.$value['Questions'].'</b>';
          echo '<br><br>';
          echo '<label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="1">'.$value['Options1'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="2">'.$value['Options2'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="3">'.$value['Options3'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="4">'.$value['Options4'].'</label>';
          echo '</li>';
        } ?>
        <li class="list-group-item">
          <input type="hidden" id="hdnMathsSubscriptionCode" value="<?php echo $mathsSubscriptionCode; ?>"/>
          <button type="button" id="saveMathsAnswers" class="btn btn-primary">Save Answers</button></li>
      </ul>
      <?php }else{ ?>
        <div class="alert alert-info">
          <strong>Info!</strong> No exam available in Science.
        </div>
      <?php } ?>
    </div>


    <div id="tab_social" class="tab-pane fade ">
      <?php if(count($arrSocialQuestions)) { ?>
      <ul class="list-group">

        <?php foreach ($arrSocialQuestions as $key => $value) {
              $socialSubscriptionCode = $value['SubscriptionCode'];
              $qno = explode('-', $value['QuestionNo']);
          echo '<li class="list-group-item questions-social" id="questions_'.$qno[0].'_'.$qno[1].'"><b>'.$qno[1].'. '.$value['Questions'].'</b>';
          echo '<br><br>';
          echo '<label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="1">'.$value['Options1'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="2">'.$value['Options2'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="3">'.$value['Options3'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="4">'.$value['Options4'].'</label>';
          echo '</li>';
        } ?>
        <li class="list-group-item">
          <input type="hidden" id="hdnSocialSubscriptionCode" value="<?php echo $socialSubscriptionCode; ?>"/>
          <button type="button" id="saveSocialAnswers" class="btn btn-primary">Save Answers</button></li>
      </ul>
      <?php } else{ ?>
        <div class="alert alert-info">
          <strong>Info!</strong> No exam available in Social.
        </div>
      <?php } ?>
    </div>


    <div id="tab_evs" class="tab-pane fade">
      <?php if(count($arrEvsQuestions)) { ?>
      <ul class="list-group">

        <?php foreach ($arrEvsQuestions as $key => $value) {
              $evsSubscriptionCode = $value['SubscriptionCode'];
              $qno = explode('-', $value['QuestionNo']);
          echo '<li class="list-group-item questions-evs" id="questions_'.$qno[0].'_'.$qno[1].'"><b>'.$qno[1].'. '.$value['Questions'].'</b>';
          echo '<br><br>';
          echo '<label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="1">'.$value['Options1'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="2">'.$value['Options2'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="3">'.$value['Options3'].'</label>
                <label class="radio-inline"><input type="radio" name="optradio_'.$qno[1].'" value="4">'.$value['Options4'].'</label>';
          echo '</li>';
        } ?>
        <li class="list-group-item">
          <input type="hidden" id="hdnEvsSubscriptionCode" value="<?php echo $evsSubscriptionCode; ?>"/>
          <button type="button" id="saveEvsAnswers" class="btn btn-primary">Save Answers</button></li>
      </ul>
      <?php } else { ?>
        <div class="alert alert-info">
          <strong>Info!</strong> No exam available in EVS.
        </div>
      <?php } ?>
    </div>


  </div>



</div>

</body>

</html>
