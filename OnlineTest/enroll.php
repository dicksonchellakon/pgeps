<?php
include_once 'config/db.php';
$database = new Database();
$dbconn = $database->getConnection();
//error_reporting(0);
date_default_timezone_set('UTC');


$StudentName = urldecode(trim($_REQUEST['StudentName']));
$AdmissionNo = trim($_REQUEST['AdmissionNo']);
$RollNo = trim($_REQUEST['RollNo']);
$Standard = trim($_REQUEST['Standard']);
$Section = trim($_REQUEST['Section']);

// select all query
$query = 'SELECT AdmissionNo from StudentsEnrolled where AdmissionNo =:AdmissionNo LIMIT 1';

// prepare query statement
$stmt = $dbconn->prepare($query);
$stmt->bindParam(":AdmissionNo", $AdmissionNo);

// execute query
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if(trim($row['AdmissionNo'])) {
  echo 'true';
  exit;
}
else {
  // query to insert record
  $query = "INSERT INTO
              StudentsEnrolled
          SET
              AdmissionNo=:AdmissionNo, RollNo=:RollNo, StudentName=:StudentName, Standard=:Standard, Section=:Section, Created=:Created";

  // prepare query
  $stmt = $dbconn->prepare($query);
  // bind values
  $stmt->bindParam(":AdmissionNo", $AdmissionNo);
  $stmt->bindParam(":RollNo", $RollNo);
  $stmt->bindParam(":StudentName", $StudentName);
  $stmt->bindParam(":Standard", $Standard);
  $stmt->bindParam(":Section", $Section);
  $stmt->bindParam(":Created", time());

  if($stmt->execute()) {
    echo 'true';
    exit;
  }
}
echo 'false';
exit;
?>
