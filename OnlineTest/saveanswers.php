<?php
include_once 'config/db.php';
$database = new Database();
$dbconn = $database->getConnection();
//error_reporting(0);
date_default_timezone_set('UTC');

$AdmissionNo = trim($_REQUEST['AdmissionNo']);
$RollNo = trim($_REQUEST['RollNo']);
$SubscriptionCode = trim($_REQUEST['SubscriptionCode']);
$Answers = utf8_encode($_REQUEST['Answer']);
$Answers = json_decode($Answers);

// select all query
$query = 'SELECT SubscriptionCode from StudentsAnswers WHERE AdmissionNo =:AdmissionNo AND RollNo=:RollNo AND SubscriptionCode=:SubscriptionCode LIMIT 1';

// prepare query statement
$stmt = $dbconn->prepare($query);
$stmt->bindParam(":AdmissionNo", $AdmissionNo);
$stmt->bindParam(":RollNo", $RollNo);
$stmt->bindParam(":SubscriptionCode", $SubscriptionCode);

// execute query
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if(trim($row['SubscriptionCode'])) { //IF Exists Update the Record

  $query = "UPDATE StudentsAnswers SET Answer=:Answer, Updated=:Updated WHERE AdmissionNo =:AdmissionNo AND RollNo=:RollNo AND SubscriptionCode=:SubscriptionCode";
  // prepare query
  $stmt = $dbconn->prepare($query);
  // bind values
  $stmt->bindParam(":AdmissionNo", $AdmissionNo);
  $stmt->bindParam(":RollNo", $RollNo);
  $stmt->bindParam(":SubscriptionCode", $SubscriptionCode);
  $stmt->bindParam(":Answer", serialize($Answers));
  $stmt->bindParam(":Updated", time());
  if($stmt->execute()) {
    echo 'true';
    exit;
  }
  echo 'false';
  exit;
}
else { //IF New INSERT the Record

  $query = "INSERT INTO
              StudentsAnswers
          SET
              AdmissionNo=:AdmissionNo, RollNo=:RollNo, SubscriptionCode=:SubscriptionCode, Answer=:Answer, Created=:Created, Updated=:Updated";

  // prepare query
  $stmt = $dbconn->prepare($query);
  // bind values
  $stmt->bindParam(":AdmissionNo", $AdmissionNo);
  $stmt->bindParam(":RollNo", $RollNo);
  $stmt->bindParam(":SubscriptionCode", $SubscriptionCode);

  $stmt->bindParam(":Answer", serialize($Answers));
  $time = time();
  $stmt->bindParam(":Created", $time);
  $stmt->bindParam(":Updated", $time);
  if($stmt->execute()) {
    echo 'true';
    exit;
  }
}
echo 'false';
exit;

?>
